// alert (`Activty 24`)

/*
1.  Create a variable getCube and use the exponent operator to compute
for the cube of a number. (A cube is any number raised to 3)

2. 	Using Template Literals, print out the value of the getCube variable with
a message of The cube of <num> is...
*/

	const cube = (num) => `The cube of ${num} is ${num ** 3}`

	const getCube = cube (2) ;
	console.log (getCube) ;

/*
3. Create a variable address with a value of an array containing details of an
address.
*/

	const address = ["258 Washington Ave" , "NW, California" , "90011"] ;

/*
4. Destructure the array and print out a message with the full address
using Template Literals.
*/

	const [addressNumber , state , zipCode] = address ;
	console.log (`I live at ${addressNumber}, ${state}, ${zipCode}.`) 


/*
5. Create a variable animal with a value of an object data type with different
animal details as it’s properties.
*/

	const animal = 
		{
			animalName: "Lolong" ,
			animalBreed: "Saltwater Crocodile" ,
			weight: 1075 ,
			measurements: "20 ft 3in" 
		}

/*
6. Destructure the object and print out a message with the details of the
animal using Template Literals.
*/

	function getAnimalData ({animalName , animalBreed, weight, measurements})
		{
			console.log (`${animalName} was a ${animalBreed}. He weighed at ${weight} kgs with a measurement of ${measurements}.`)
		}
	getAnimalData (animal) ;


/*
7. Create an array of numbers.
*/

	const numbers = [1 ,2, 3, ,4, 5] ;

/*
8. Loop through the array using forEach, an arrow function and using the
implicit return statement to print out the numbers.
*/

	numbers.forEach ((num) => console.log (num)); 

/*
9. Create a class of a Dog and a constructor that will accept a name, age and
breed as it’s properties.
*/
	class Dog
		{
			constructor (name, age, breed)
				{
				this.dogName = name ;
				this.dogAge = age ; 
				this.dogBreed = breed
				}
		}


/*
10. Create/instantiate a new object from the class Dog and console log the
object. 
*/

	const doggy = new Dog ("Frankie" , 5 , "Miniature Dachshund") ;
	console.log (doggy) ;

// DONE!